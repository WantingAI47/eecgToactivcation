import tensorflow as tf


length = 500
channels = 32


def cnnmodel(x, keep_prob_, alpha, training=True):
    x = tf.reshape(x, [-1, length, channels])
    conv1 = tf.layers.separable_conv1d(x,filters=32, kernel_size=5, strides=1, padding='SAME', data_format='channels_first',
                                       activation=tf.nn.elu,trainable=training,
                                       kernel_initializer=tf.truncated_normal_initializer(stddev=0.01),
                                       kernel_regularizer=tf.contrib.layers.l2_regularizer(0.003), name='conv1')
    pool1 = tf.layers.max_pooling1d(inputs=conv1, pool_size=3, strides=3, padding='same')
    bn1 = tf.layers.batch_normalization(inputs=pool1, axis=1, training=training)
    conv2 = tf.layers.separable_conv1d(bn1, filters=64, kernel_size=7, strides=1, padding='same', data_format='channels_first',
                                       activation=tf.nn.elu,trainable=training,
                                       kernel_initializer=tf.truncated_normal_initializer(stddev=0.01),
                                       kernel_regularizer=tf.contrib.layers.l2_regularizer(0.003),name='conv2')
    pool2 = tf.layers.max_pooling1d(inputs=conv2, pool_size=3,strides=3,padding='same')
    bn2 = tf.layers.batch_normalization(inputs=pool2, axis=1, training=training,name='bn2')
    conv3 = tf.layers.separable_conv1d(bn2, filters=128, kernel_size=9,strides=1, padding='SAME', data_format='channels_first',
                                       activation=tf.nn.elu, trainable=training,
                                       kernel_initializer=tf.truncated_normal_initializer(stddev=0.01),
                                       kernel_regularizer=tf.contrib.layers.l2_regularizer(0.003), name='conv3')
    pool3 = tf.layers.max_pooling1d(inputs=conv3, pool_size=3, strides=3, padding='same')
    bn3 = tf.layers.batch_normalization(inputs=pool3, axis=1, training=training, name='bn3')
    drop1 = tf.nn.dropout(bn3, keep_prob=keep_prob_)
    conv4 = tf.layers.separable_conv1d(drop1, filters=256, kernel_size=11, strides=1, padding='SAME',
                                       data_format='channels_first',
                                       activation=tf.nn.elu, trainable=training,
                                       kernel_initializer=tf.truncated_normal_initializer(stddev=0.01),
                                       kernel_regularizer=tf.contrib.layers.l2_regularizer(0.003),
                                       name='conv4')
    pool4 = tf.layers.max_pooling1d(inputs=conv4, pool_size=3, strides=3, padding='same')
    fc1 = tf.layers.dense(pool4, 256)
    if alpha != 0:
        fc1 = alpha * tf.divide(fc1, tf.norm(fc1, ord='euclidean'))
    drop2 = tf.nn.dropout(fc1, keep_prob=keep_prob_)
    logits = tf.layers.dense(drop2, 6)
    regularization_loss = tf.reduce_sum(tf.get_collection(tf.GraphKeys.REGULARIZATION_LOSSES))
    return logits, regularization_loss





