import tensorflow as tf
import readdata
import model
from sklearn.model_selection import train_test_split
import numpy as np
from tensorflow.python.framework import graph_util

pb_file_path = 'F:/eegclassfier/cnnmodel.pb'
x = tf.placeholder(tf.float32, shape=[None, 32, 500], name='inx')
y = tf.placeholder(tf.float32, shape=[None, 6])
keep_prob_ = tf.placeholder(tf.float32, name='keep')
learning_rate_ = tf.placeholder(tf.float32, name='learning_rate')
training = tf.placeholder(tf.bool,name='training')

epochs = 30
batch_size = 64

logits, regularization_loss = model.cnnmodel(x, keep_prob_, alpha=5,training=training)
with tf.name_scope('cost'):
    cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=logits, labels=y)) + regularization_loss
    tf.summary.scalar('cost', cost)
optimizer = tf.train.AdamOptimizer(learning_rate_).minimize(cost)
label_pr = tf.argmax(logits, 1, name='prlabel')
# Accuracy
correct_pred = tf.equal(tf.argmax(logits, 1), tf.argmax(y, 1))
confusion_matrix = tf.confusion_matrix(tf.argmax(y, 1), tf.argmax(logits, 1), num_classes=3)
with tf.name_scope('accuracy'):
    accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32), name='accuracy')
    tf.summary.scalar('accuracy', accuracy)

with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())
    for e in range(epochs):
        X_train,y_train = readdata.readtraindata(1)
        train_data, test_data, train_lables, test_lables = train_test_split(X_train, y_train, test_size=0)
        for i in range(np.ceil(train_lables[0]/batch_size)):
            x_batch, y_batch = readdata.get_batches(i,train_data,train_lables,batch_size)
            feed = {x: x_batch, y: y_batch, keep_prob_: 0.8, learning_rate_: 0.0001,training:True}
            loss, _, acc = sess.run([cost, optimizer, accuracy], feed_dict=feed)
            if (i+1) % 5 == 0:
                print("Epoch: {}/{}".format(e, epochs), "Iteration: {:d}".format(i+1),
                      "Train loss: {:6f}".format(loss), "Train acc: {:.6f}".format(acc))
            if (i+1) % 20 == 0:
                test_input = readdata.readtestdata(1)
                feed = {x : test_input, keep_prob_:1,training:False}
                loss_v, _, acc_v, pl, matrix = sess.run([cost, optimizer, accuracy, label_pr, confusion_matrix],
                                                        feed_dict=feed)
                print("Epoch: {}/{}".format(e, epochs), "Iteration: {:d}".format(i+1),
                      "Validation loss: {:6f}".format(loss_v), "Validation acc: {:.6f}".format(acc_v))
                print('confusion matric:')
                print(matrix)

    constant_graph = graph_util.convert_variables_to_constants(sess, sess.graph_def,
                                                               ["inx", 'prlabel', 'keep','training'])
    with tf.gfile.FastGFile(pb_file_path, mode='wb') as f:
        f.write(constant_graph.SerializeToString())




