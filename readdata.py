import numpy as np
import pandas as pd
from glob import glob
from sklearn.preprocessing import StandardScaler



def prepare_data_train(fname):
    """ read and prepare training data """
    # Read data
    data = pd.read_csv(fname)
    # events file
    events_fname = fname.replace('_data','_events')
    # read event file
    labels = pd.read_csv(events_fname)
    clean = data.drop(['id' ], axis=1)#remove id
    labels = labels.drop(['id' ], axis=1)#remove id
    return clean, labels


def prepare_data_test(fname):
    """ read and prepare test data """
    # Read data
    data = pd.read_csv(fname)
    return data

scaler= StandardScaler()


def data_preprocess_train(X):
    X_prep=scaler.fit_transform(X)
    return X_prep


def data_preprocess_test(X):
    X_prep=scaler.transform(X)
    return X_prep

def readtraindata(id):
    y_raw = []
    raw = []
    fnames = glob('C:/Users/zhenghuimin/Downloads/all/train/train/subj%d_series*_data.csv' % (id))
    for fname in fnames:
      data,labels = prepare_data_train(fname)
      raw.append(data)
      y_raw.append(labels)
    X = pd.concat(raw)
    y = pd.concat(y_raw)
    X_train = np.asarray(X.astype(float))
    y = np.asarray(y.astype(float))
    return X_train, y


def readtestdata(id):
    fnames = glob('C:/Users/zhenghuimin/Downloads/all/test/test/subj%d_series*_data.csv' % (id))
    test = []
    idx = []
    for fname in fnames:
        data = prepare_data_test(fname)
        test.append(data)
        idx.append(np.array(data['id']))
    X_test = pd.concat(test)
    X_test = X_test.drop(['id'], axis=1)  # remove id
    X_test = np.asarray(X_test.astype(float))
    return X_test


def get_batches(iterations, x, y, batch_size):
    length = x.shape[0]
    batches_wavs = []
    batches_labels = []
    if iterations * batch_size < length:
        for j in range(batch_size):
            dsd = x[(iterations - 1) * batch_size + j]
            batches_wavs.append(dsd)
            batches_labels.append(y[(iterations - 1) * batch_size + j])
    else:
        for j in range(batch_size):
            dsd = x[(iterations - 1) * batch_size + j - length]
            batches_wavs.append(dsd)
            batches_labels.append(y[(iterations - 1) * batch_size + j - length])
    return batches_wavs, batches_labels


